// remise au propre :
// Declaration variable et constante :

const listeImg = [
  "pictures/img01.jpg",
  "pictures/img01.jpg",
  "pictures/img02.jpg",
  "pictures/img02.jpg",
  "pictures/img03.jpg",
  "pictures/img03.jpg",
  "pictures/img04.jpg",
  "pictures/img04.jpg",
  "pictures/img05.jpg",
  "pictures/img05.jpg",
  "pictures/img06.jpg",
  "pictures/img06.jpg",
  "pictures/img07.jpg",
  "pictures/img07.jpg",
  "pictures/img08.jpg",
  "pictures/img08.jpg",
  "pictures/img09.jpg",
  "pictures/img09.jpg",
  "pictures/img10.jpg",
  "pictures/img10.jpg",
  "pictures/img11.jpg",
  "pictures/img11.jpg",
  "pictures/img12.jpg",
  "pictures/img12.jpg",
];
const level = document.getElementById("level");
const levelChoice = document.querySelectorAll("option");
const btnStart = document.getElementById("btnStart");

const carteRecouverte = document.getElementsByClassName("pictures");
let count = 0,
  nbPaireTrouve = 1;
let premiereCarteAttribut,
  deuxiemeCarteAttribut,
  firstCardIndex,
  secondCardIndex;

const container = document.getElementById("container");
let pictures;
let choice = "";
let easy = 12,
  medium = 18,
  hard = 24;
let listImgLevel;
let levelSelected;

// ecoute des evenements :
// ecoute du choix de niveau :

levelChoice.forEach((element) => {
  element.addEventListener("click", () => {
    choice = element.getAttribute("value");
    if (choice == "easy") {
      levelSelected = easy;
    } else {
      if (choice == "medium") {
        levelSelected = medium;
      } else {
        if (choice == "hard") {
          levelSelected = hard;
        }
      }
    }
  });
});

// Déclaration des fonctions:

function createDiv() {
  for (let i = 0; i < levelSelected; i++) {
    let newDiv = document.createElement("div");
    newDiv.setAttribute("class", "pictures");
    let newImg = document.createElement("img");
    newImg.setAttribute("class", "origine");
    newImg.setAttribute("src", "");
    container.appendChild(newDiv);
    newDiv.appendChild(newImg);
    newImg.style.border = "1px solid blue";
  }
}

function insertAleatoryImg() {
  if (choice == "easy") {
    pictures.forEach((element) => {
      // recupère un nombre aléatoire entre 1 et nombre d'image -1 arrondi à l'entier sup
      let indiceAleatoire = Math.random() * listImgLevel.length;
      let indiceAleatoireArrondi = Math.ceil(indiceAleatoire - 1);
      // on met l'image en opacité 0 et on insère le chemin de destination de l'image grace au nombre aléatoire qui sert d'index du tableau et on retire cette valeur du tableau pour ne pas tomber 2 fois sur la même image
      element.style.opacity = "0";
      element.setAttribute("src", listImgLevel[indiceAleatoireArrondi]);

      listImgLevel.splice(indiceAleatoireArrondi, 1);
    });
    count = 0;
  } else {
    if (choice == "medium") {
      pictures.forEach((element) => {
        // recupère un nombre aléatoire entre 1 et nombre d'image -1 arrondi à l'entier sup
        let indiceAleatoire = Math.random() * listImgLevel.length;
        let indiceAleatoireArrondi = Math.ceil(indiceAleatoire - 1);
        // on met l'image en opcatité 0 et on insère le chemin de destination de l'image grace au nombre aléatoire qui sert d'index du tableau et on retire cette valeur du tableau pour ne pas tomber 2 fois sur la même image
        element.style.opacity = "0";

        element.setAttribute("src", listImgLevel[indiceAleatoireArrondi]);
        listImgLevel.splice(indiceAleatoireArrondi, 1);
      });
      count = 0;
    } else {
      if (choice == "hard") {
        pictures.forEach((element) => {
          // recupère un nombre aléatoire entre 1 et nombre d'image -1 arrondi à l'entier sup
          let indiceAleatoire = Math.random() * listImgLevel.length;
          let indiceAleatoireArrondi = Math.ceil(indiceAleatoire - 1);
          // on met l'image en opcatité 0 et on insère le chemin de destination de l'image grace au nombre aléatoire qui sert d'index du tableau et on retire cette valeur du tableau pour ne pas tomber 2 fois sur la même image
          element.style.opacity = "0";

          element.setAttribute("src", listImgLevel[indiceAleatoireArrondi]);
          listImgLevel.splice(indiceAleatoireArrondi, 1);
        });
        count = 0;
      }
    }
  }
}

// ecoute click bouton demarrer:
btnStart.addEventListener("click", runGame);
function runGame() {
  while (container.firstChild) {
    container.removeChild(container.firstChild);
  }
  nbPaireTrouve = 1;
  createDiv();

  listImgLevel = listeImg.slice(0, levelSelected);

  pictures = document.querySelectorAll("img");
  insertAleatoryImg();

  pictures.forEach((element) => {
    element.addEventListener("click", devoileCarte);
    function devoileCarte() {
      element.classList.toggle("rotate");

      if (Event) {
        if (nbPaireTrouve == pictures.length / 2) {
          if (element.style.opacity == "0") {
            element.style.opacity = "1";
            alert("Félicitation!!! Vous avez réussi");

            // pictures.forEach((element) => {
            //   Inserer regle animation suite a la reussite
            // });
          }
        } else {
          if (element.style.opacity == "0") {
            if (count == 0) {
              element.style.opacity = "1";
              premiereCarte = element.getAttribute("src");
              firstCardIndex = element;
              count++;
            } else {
              if (count == 1) {
                element.style.opacity = "1";
                deuxiemeCarte = element.getAttribute("src");
                secondCardIndex = element;
                count++;
              } else {
                // Définir ici la règle de comparaison des deux images retournées
                if (count == 2) {
                  if (premiereCarte !== deuxiemeCarte) {
                    firstCardIndex.style.opacity = "0";
                    secondCardIndex.style.opacity = "0";
                    count = 0;
                    element.style.opacity = "1";
                    premiereCarte = element.getAttribute("src");
                    firstCardIndex = element;
                    count++;
                  } else {
                    count = 0;

                    element.style.opacity = "1";
                    premiereCarte = element.getAttribute("src");
                    firstCardIndex = element;

                    count++;
                    nbPaireTrouve++;
                  }
                }
              }
            }
          }
        }
      }
    }
  });
}

// script pour animer retournement des cartes :

// pictures.forEach((element) => {
//   element.addEventListener("click", turnAround);
// function turnAround() {
//   element.classList.toggle("rotate");
// }
// });

// partie pour configurer le mode Dark ou Light :
const btnMode = document.querySelector(".light");
const bodyMode = document.querySelector(".bodyLight");
// const btnStart = document.querySelector("#btn .light");
btnMode.addEventListener("click", switchMode);
function switchMode() {
  btnMode.classList.toggle("dark");
  bodyMode.classList.toggle("bodyDark");
  btnStart.classList.toggle("dark");
  level.classList.toggle("dark");
  if (btnMode.innerHTML == "Mode Light") {
    btnMode.innerHTML = "Mode Dark";
  } else {
    btnMode.innerHTML = "Mode Light";
  }
}
